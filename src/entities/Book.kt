package entities

import java.util.*

class Book (val id: String? = UUID.randomUUID().toString(), val author: String, val title:String, val genre:String) {
    override fun toString(): String {
        return "Book(id='$id', authhor='$author', title='$title', genre='$genre')"
    }
}

