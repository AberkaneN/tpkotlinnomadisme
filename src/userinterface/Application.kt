package userinterface

import entities.Book
import entities.Library
import external.Export
import usecase.Management

fun main() {
    val booksbibiliotheque = mutableListOf<Book>();
    val library = Library(booksbibiliotheque);
    val exportInterface = Export()
    val management= Management(library,exportInterface);
    println("Bienvenue dans votre bibliothèque")
    while (true) {
        val choix = menu()
        when (choix) {
            "add" -> {
                management.addBookToLibrary()
            }
            "del" -> {
                management.deleteBook()
            }
            "list" -> {
                management.listExistingBook()
            }
            "count" -> {
                management.countBook()
            }
            "export" -> {
                management.export()
            }
        }
    }
}

fun menu(): String? {
    println("saisir un de ces choix add / del / count /list / export")
    val choix = readLine()
    return choix;
}
