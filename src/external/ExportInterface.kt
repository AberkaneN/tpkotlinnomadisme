package external

import entities.Library

interface ExportInterface {
    fun exportLibrary(library: Library)
}