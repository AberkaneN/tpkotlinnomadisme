package external

import java.io.File
import com.google.gson.GsonBuilder
import entities.Library

class Export : ExportInterface {
    override fun exportLibrary(library: Library) {
        val gsonPretty = GsonBuilder().setPrettyPrinting().create()
        val jsonTutsListPretty: String = gsonPretty.toJson(library.books)
        File("export.json").writeText(jsonTutsListPretty)
    }
}