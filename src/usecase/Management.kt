package usecase

import entities.Book
import entities.Library
import external.ExportInterface
import java.util.*

class Management(val library: Library, val exportInterface: ExportInterface)  {

    fun addBookToLibrary() {
        println("Auteur:")
        val auteur = readLine()
        println("$auteur")
        println("Titre:")
        val titre = readLine()
        println("$titre")
        println("Genre:")
        val genre = readLine()
        println("$genre")
        val book = Book(
            UUID.randomUUID().toString(),
            auteur.toString(),
            titre.toString(),
            genre.toString()
        )
        library.books.add(book)
    }

    fun listExistingBook() {

        if (library.books.count() != 0) {
            println("La bibliothèque contient à présent les livres suivants :")
            library.books.forEach { book -> println(book) }
        } else {
            println("Aucun livre")
        }
    }

    fun deleteBook() {
        println("Saisir Identifiant livre")
        val identifiantLivre = readLine()
        library.books.removeIf { book -> book.id.equals(identifiantLivre) }
    }

    fun countBook() {
        println("nombre de livre " + library.books.count())
    }

    fun export(){
        exportInterface.exportLibrary(this.library)
    }


}